import logo from "./logo.svg";
import "./App.css";
import BTForm from "./Component/BTForm";

function App() {
  return (
    <div className="App">
      <BTForm />
    </div>
  );
}

export default App;
