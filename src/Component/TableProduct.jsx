import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { btFormSVActions } from "../store/FormSV/slice";

const TableProduct = () => {
  const { productList } = useSelector((state) => state.btFormSV);
  // console.log("productList :", productList);

  const dispatch = useDispatch();

  // tình năng search: lấy thông tin của ô input gửi lên store => so sánh TỪNG CHỮ = hàm filter, includes
  const [searchData, setSearchData] = useState();
  // console.log("searchData :", searchData);

  const handleSearchForm = () => (ev) => {
    const { name, value } = ev.target;
    setSearchData({
      [name]: value,
    });
  };
  handleSearchForm();
  return (
    <div className="mt-3">
      <div className="mt-4">
        <input
          type="text"
          placeholder="Search in here"
          name="search"
          onChange={handleSearchForm()}
        />

        <button
          className="btn-outline-dark btn"
          onClick={() => dispatch(btFormSVActions.search(searchData))}
        >
          Search
        </button>
      </div>
      <table className="table">
        <thead>
          <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Số điện thoại</td>
            <td>Email</td>
            <td></td>
          </tr>
        </thead>
        <tbody>
          {productList?.map((prd) => (
            <tr key={prd?.id}>
              <td>{prd?.id}</td>
              <td>{prd?.name}</td>
              <td>{prd?.phone}</td>
              <td>{prd?.email}</td>
              <td>
                <button
                  className="btn btn-success"
                  onClick={() => {
                    dispatch(btFormSVActions.deleteProduct(prd.id));
                  }}
                >
                  Delete
                </button>
                <button
                  className="btn btn-info ms-2"
                  onClick={() => {
                    dispatch(btFormSVActions.editProduct(prd));
                  }}
                >
                  Edit
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TableProduct;
