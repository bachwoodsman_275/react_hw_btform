import React from "react";
import FormProduct from "./FormProduct";
import TableProduct from "./TableProduct";

const BTForm = () => {
  return (
    <div>
      <h1>BTForm</h1>
      <FormProduct />
      <TableProduct />
    </div>
  );
};

export default BTForm;
