import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { btFormSVActions } from "../store/FormSV/slice";

const FormProduct = () => {
  // Dispatch hook
  const dispatch = useDispatch();

  // useSelector hook
  const { productEdit } = useSelector((state) => state.btFormSV);
  // console.log("productEdit :", productEdit);

  // UseState
  const [formData, setFormData] = useState();
  // console.log("formData :", formData);

  const [formError, setFormError] = useState();
  // console.log("formError :", formError);

  // Handle Function: người dùng nhập giá trị thì hàm handle mới được chạy
  const handleFormData = () => (ev) => {
    const { name, value, title, minLength, max, maxLength, validity } =
      ev.target;
    // console.log("validity :", validity);
    // console.log("minLength :", minLength);

    // setFormData ban đầu để người dùng nhập và xóa
    setFormData({
      ...formData,
      [name]: value,
    });

    // Validation input
    let mess;
    // nếu minLength có giá trị và giá trị trong ô input là rỗng thì if chạy
    if (minLength !== -1 && !value.length) {
      mess = `Vui lòng nhập thông tin ${title}`; // dùng template string để có thể thay đổi theo từng ô input khác nhau
    } else if (validity.patternMismatch && ["id", "phone"].includes(name)) {
      mess = "Vui lòng nhập ký tự là số";
    } else if (value.length < minLength) {
      mess = `Vui lòng nhập tối thiểu ${minLength} ký tự`;
    } else if (value.length > max && max) {
      // vừa kiểm tra độ dài vượt quá max và max phải tồn tại
      mess = `Tối đa ${max} ký tự`;
    } else if (validity.patternMismatch && name === "email") {
      mess = "Vui lòng nhập đúng email";
    }
    setFormError({
      ...formError, // Chồng các key lại với nhau, giả sử ta nhập id và email thì formError sẽ có 2 thuộc tính là id và email
      [name]: mess, // thuộc tính id, name, email,... sẽ có giá trị là mess câu thông báo lỗi
    });
    // console.log("formError :", formError);

    // console.log("mess :", mess);

    if (mess) return; // Khi user không nhập gì thì mess có giá trị => return

    // setFormData có điều kiện nếu mess không có giá trị (= undefined) thì setFormData sẽ được cập nhật
    // nếu mess có giá trị thì return rồi làm sao chạy được setFormData mà kiểm tra mess ?
    // nếu mess không giá trị thì xuống dưới mess được kiểm tra nếu mess đúng thì mess = undefined và nếu mess sai thì mess = value
    setFormData({
      ...formData, // Giúp lưu lại kết quả của id và name hoặc,.. khi gõ qua input khác
      [name]: mess ? undefined : value, //khi gõ tiếp trong input nếu value là productEdit thì khi sự kiện onChange sẽ render lại component => giá trị value vẫn sẽ là productEdit
    });
  };

  // UseEffect hook
  useEffect(() => {
    if (!productEdit) return; // nếu mới tạo lại trang thì productEdit chưa có giá trị
    setFormData(productEdit); // nếu đã có giá trị thì productEdit sẽ thay thể cho formData để hiển thị lên ô input
  }, [productEdit]);
  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        let flag;
        for (let key in formError) {
          // key này là các thuộc tính id, namem,... formError có chứa các thuộc tính này thì flag = true
          if (formError[key]) {
            flag = true;
            break;
          }
        }
        if (flag) return;

        // Kiểm tra nếu chưa có chỉnh sửa thì productEdit chưa có giá trị => thêm product
        if (productEdit) {
          dispatch(btFormSVActions.updateProductEdit(formData)); // truyền lên store 1 product cần update
        } else {
          dispatch(btFormSVActions.addProduct(formData));
        }
      }}
      noValidate // Có validate nhưng vẫn có thể submit và tắt các pop up validate của browser
    >
      <h2 className="px-2 py-4 bg-dark text-warning">Thông Tin Sinh Viên</h2>
      <div className="form-group row">
        <div className="col-6">
          <p>Mã Sinh Viên</p>
          <input
            type="text"
            disabled={!!productEdit} // Nếu productEdit có giá trị thì ô input id sẽ disable, !! sẽ đổi giá trị thành kiểu boolean
            value={formData?.id}
            minLength={1}
            max={5} // Được nhập quá giá trị max
            // maxLength={6} // Không được nhập quá giá trị max
            pattern="^[0-9]+$"
            name="id"
            title="id"
            className="form-control"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.id}</p>
        </div>
        <div className="col-6">
          <p>Họ Tên</p>
          <input
            type="text"
            minLength={2}
            value={formData?.name}
            name="name"
            title="tên"
            className="form-control"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.name}</p>
        </div>
      </div>
      <div className="form-group row">
        <div className="col-6">
          <p>Số điện thoại</p>
          <input
            type="text"
            minLength={3}
            value={formData?.phone}
            pattern="^[0-9]+$"
            name="phone"
            title="số điện thoại"
            className="form-control"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.phone}</p>
        </div>
        <div className="col-6">
          <p>Email</p>
          <input
            type="text"
            value={formData?.email}
            minLength={2}
            pattern="^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"
            name="email"
            title="email"
            className="form-control"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.email}</p>
        </div>
      </div>
      <div className="mt-3">
        <button className="btn btn-success py-2 ms-2">
          Gửi thông tin sinh viên
        </button>
      </div>
    </form>
  );
};

export default FormProduct;
