import { combineReducers } from "redux";
import { btFormSVreducer } from "./FormSV/slice";

export const rootReducer = combineReducers({
  btFormSV: btFormSVreducer,
});
