import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  productList: [],
  productEdit: undefined,
};

const btSliceSV = createSlice({
  name: "btFormSV",
  initialState,
  reducers: {
    addProduct: (state, action) => {
      state.productList.push(action.payload);
    },
    deleteProduct: (state, { payload }) => {
      state.productList = state.productList.filter((prd) => prd.id !== payload);
    },
    editProduct: (state, { payload }) => {
      // payload sẽ được truyền vào 1 item
      state.productEdit = payload;
    },
    updateProductEdit: (state, { payload }) => {
      // payload sẽ được truyền vào là 1 item product
      state.productList = state.productList.map((prd) => {
        if (prd.id == payload.id) {
          return payload;
        }
        return prd;
      });
      state.productEdit = undefined; // sau khi update thành công thì sẽ gán giá trị productEdit = undefined
    },
    search: (state, { payload }) => {
      console.log("payload :", payload);

      // Tạo 1 mảng dùng spread oprator copy lại productList
      const productListCopy = [...state.productList];
      // payload : 1 obj người dùng gõ vào input search, payload.search các ký tự gõ trong input
      // Nếu payload có giá trị thì gọi hàm filter còn payload = undefined thì gọi lại hàm productList
      if (payload) {
        // Filter ra các product có các chữ đúng với các chữ người dùng gõ và nhấn search
        state.productList = productListCopy.filter((prd) =>
          prd.name.toLowerCase().includes(payload.search.toLowerCase())
        );
      } else {
        return productListCopy;
      }
    },
  },
});

export const { actions: btFormSVActions, reducer: btFormSVreducer } = btSliceSV;
